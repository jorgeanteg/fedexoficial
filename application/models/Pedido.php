<?php

class Pedido extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insertar($datos)
  {
    return $this->db->insert("pedido", $datos);
  }

  function obtenerTodosListaU()
  {
    $this->db->select('id_ped, nombre_ped, descripcion_ped,fecha_ped, pais_destino_ped, latitud_destino_ped,longitud_destino_ped,codigo_ped,apellido_cli,nombre_cli, nombre_suc');
    $this->db->from('pedido');
    $this->db->join('cliente', 'cliente.id_cli = pedido.id_fk_cli');
    $this->db->join('sucursal', 'sucursal.id_suc = pedido.id_fk_suc');
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }

  function obtenerTodos()
  {
    $listadoPedidos = $this->db->get("pedido");
    if ($listadoPedidos->num_rows() > 0) {
      return $listadoPedidos->result();
    }
    return false;
  }

  function buscar($query)
  {
    return $this->db->like('codigo_ped', $query)->get('pedido')->result();
  }

  function borrar($id_ped)
  {
    $this->db->where("id_ped", $id_ped);
    return $this->db->delete("pedido");
  }


  function obtenerPorId($id_ped)
  {
    //"id_ins"-> es el campo de la base de datos  y la $id_ins es la variable que creamos puede tener otro nombre
    $this->db->where("id_ped", $id_ped);

    $pedido = $this->db->get("pedido");
    //instructor tabla de base de datos
    if ($pedido->num_rows() > 0) {
      return $pedido->row();
    }
    return false;
  }

  function obtenerRutaPedido($id_ped)
  {
    $this->db->select('cliente.nombre_cli, cliente.apellido_cli,cliente.telefono_cli, cliente.cedula_cli, sucursal.nombre_suc, sucursal.latitud_suc, sucursal.longitud_suc, sucursal.pais_suc, sucursal.ciudad_suc,sucursal.telefono_suc, pedido.codigo_ped, pedido.nombre_ped, pedido.latitud_destino_ped, pedido.descripcion_ped, pedido.longitud_destino_ped, pedido.pais_destino_ped, pedido.ciudad_destino_ped');
    $this->db->from('pedido');
    $this->db->join('cliente', 'cliente.id_cli = pedido.id_fk_cli');
    $this->db->join('sucursal', 'sucursal.id_suc = pedido.id_fk_suc');
    $this->db->where('pedido.id_ped', $id_ped);
    $query = $this->db->get();
    return $query->row(); // Retorna una única fila con los datos del pedido
  }

  function obtenerUltimoCodigoPedido()
  {
    $this->db->select('codigo_ped');
    $this->db->from('pedido');
    $this->db->order_by('id_ped', 'DESC');
    $this->db->limit(1);
    $query = $this->db->get();

    if ($query->num_rows() > 0) {
      $row = $query->row();
      return $row->codigo_ped;
    } else {
      return false;
    }
  }

      function actualizar($id_ped,$datos)
      {
        $this->db->where("id_ped",$id_ped);
        return $this->db->update('pedido',$datos);
      }

}
?>
