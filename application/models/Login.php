<?php
class Login extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function login($username, $password) {
        $this->db->where('email_user', $username);
        $this->db->where('password_user', $password);
        $query = $this->db->get('usuario');
        
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
}
?>
