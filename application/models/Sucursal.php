<?php
class Sucursal extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }


    //funcion insertar un cliente
    function insertar($datos){
        //ACTIVE RECORD -> CODEiGNITER
        return $this->db->insert("sucursal",$datos);
    }

    //FUncion para consultar Instructores
    function obtenerTodos(){
        $listadoSucursales=$this->db->get("sucursal");//esto devuelve un array
        if($listadoSucursales->num_rows()>0) { //si hay datos
            return $listadoSucursales->result();
        }else{ //si no hay datos
            return false;
        }
    }

    //Borrar cliente
    function borrar($id_cli){
        //"id_ins"-> es el campo de la base de datos  y la $id_ins es la variable que creamos puede tener otro nombre
        $this->db->where("id_suc",$id_cli);

        //cliente tabla de base de datos
        if ($this->db->delete("sucursal")) {
            return true;
        } else {
            return false;
        }

    }

    public function obtener_sucursal_por_pais($pais) {
        $this->db->where('pais_suc', $pais);
        $query = $this->db->get('sucursal');
        return $query->result();
    }

    function obtenerPorId($id_suc){
      $this->db->where("id_suc",$id_suc);
      $sucursal=$this->db->get("sucursal");
      if ($sucursal->num_rows()>0) {
        return $sucursal->row();
      }
      return false;
    }

    function actualizar($id_suc,$datos)
    {
      $this->db->where("id_suc",$id_suc);
      return $this->db->update('sucursal',$datos);
    }

} //cierre de la clase


?>
