<?php
class Cliente extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }


    //funcion insertar un cliente
    function insertar($datos){
        //ACTIVE RECORD -> CODEiGNITER
        return $this->db->insert("cliente",$datos);
    }

    //FUncion para consultar Instructores
    function obtenerTodos(){
        $listadoClientes=$this->db->get("cliente");//esto devuelve un array
        if($listadoClientes->num_rows()>0) { //si hay datos
            return $listadoClientes->result();
        }else{ //si no hay datos
            return false;
        }
    }

    //Borrar cliente
    function borrar($id_cli){
        //"id_ins"-> es el campo de la base de datos  y la $id_ins es la variable que creamos puede tener otro nombre
        $this->db->where("id_cli",$id_cli);

        //cliente tabla de base de datos
        if ($this->db->delete("cliente")) {
            return true;
        } else {
            return false;
        }

    }

    public function obtener_clientes_por_pais($pais) {
        $this->db->where('pais_cli', $pais);
        $query = $this->db->get('cliente');
        return $query->result();
    }

    function obtenerPorId($id_cli){
      $this->db->where("id_cli",$id_cli);
      $cliente=$this->db->get("cliente");
      if ($cliente->num_rows()>0) {
        return $cliente->row();
      }
      return false;
    }

    function actualizar($id_cli,$datos)
    {
      $this->db->where("id_cli",$id_cli);
      return $this->db->update('cliente',$datos);
    }

} //cierre de la clase


?>
