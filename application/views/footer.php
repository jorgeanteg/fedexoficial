<!-- Footer Start -->
<div class="footer wow fadeIn" data-wow-delay="0.3s">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="footer-contact">
                    <h2>Contáctos</h2>
                    <p><i class="fa fa-map-marker-alt"></i>Matriz, Quito, Ecuador</p>
                    <p><i class="fa fa-phone-alt"></i>+012 345 67890</p>
                    <p><i class="fa fa-envelope"></i>fedex@gmail.com</p>
                    <div class="footer-social">
                        <a href="https://twitter.com/fedexteayuda"><i class="fab fa-twitter"></i></a>
                        <a href="https://www.facebook.com/FedEx/?locale=es_LA"><i class="fab fa-facebook-f"></i></a>
                        <a href="https://www.youtube.com/user/fedex"><i class="fab fa-youtube"></i></a>
                        <a href="https://www.instagram.com/fedex/?hl=es"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="footer-link">
                    <h2>Servicios de Envío</h2>
                    <p>Documentos</p>
                    <p>Mercancías</p>
                    <p>ServiFarma</p>
                    <p>Empaque y Embalaje</p>
                    <p>Almacenamiento</p>

                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="newsletter">
                    <h2>Nuestro Eslógan Empresarial</h2>
                    <p>
                        En FedEx, entregamos más que paquetes. Entregamos confianza.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container copyright">
        <div class="row">
            <div class="col-md-12 text-center">
                <p>&copy; <a href="#">FEDEX</a>, Todos los derechos reservados.</p>
                <p>Designed By Sistemas UTC</a></p>
            </div>

        </div>
    </div>
</div>
<!-- Footer End -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
</div>

<?php if ($this->session->flashdata("confirmacion")): ?>
    <script type="text/javascript">
        toastr.success("<?php echo $this->session->flashdata("confirmacion"); ?>", '', { "positionClass": "toast-bottom-right" });
    </script>
    <?php $this->session->set_flashdata("confirmacion", "") ?>
<?php endif; ?>

<?php if ($this->session->flashdata("error")): ?>
    <script type="text/javascript">
        toastr.error("<?php echo $this->session->flashdata("error"); ?>", '', { "positionClass": "toast-bottom-right" });
    </script>
    <?php $this->session->set_flashdata("error") ?>
<?php endif; ?>
<!-- JavaScript Libraries -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/lib/easing/easing.min.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/lib/wow/wow.min.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/lib/isotope/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/lib/lightbox/js/lightbox.min.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/lib/waypoints/waypoints.min.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/lib/counterup/counterup.min.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/lib/slick/slick.min.js"></script>

<!-- Template Javascript -->
<script src="<?php echo base_url(); ?>/plantilla/assets/js/main.js"></script>

<!-- Importacion de Bootstrap -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz"
    crossorigin="anonymous"></script>


</body>

</html>