<style>
  #map {
    height: 400px;
    width: 100%;
  }
</style>
<!-- Service Start -->
<div class="service">
  <div class="container">
    <div class="section-header text-center">
      <br>
      <h2>Detalles del Pedido</h2>
      <h2>Geolocalización del pedido:
        <?php echo $pedido->codigo_ped; ?>
      </h2>
    </div>
    <div class="text-center">
      <a href="<?php echo site_url(); ?>/pedidos/buscarpedido" class="btn btn-primary">Buscar otro Envío</a>
    </div>
    <br>


    <div class="row align-items-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header wow fadeInUp" data-wow-delay="0.1s">
            <br>
            <div class="text-center">
              <h5>Pedido:
                <?php echo $pedido->nombre_ped; ?>
              </h5>
            </div>
            <span>Se presenta la ruta por la cual se transportará su pedido</span>
            <br>
            <p><strong>Origen:</strong>
              <?php echo $pedido->ciudad_suc; ?>-
              <?php echo $pedido->pais_suc; ?> --------------------------------------------------------------->
              <strong>Destino:</strong>
              <?php echo $pedido->ciudad_destino_ped; ?>-
              <?php echo $pedido->pais_destino_ped; ?>
            </p>

            <div class="row">
              <div class="col-md-4">
                <p>
                  <strong>Cédula:</strong>
                  <?php echo $pedido->cedula_cli; ?>
                <p>
              </div>
              <div class="col-md-4">
                <p>
                  <strong>Cliente:</strong>
                  <?php echo $pedido->nombre_cli; ?>
                  <?php echo $pedido->apellido_cli; ?>
                <p>
              </div>
              <div class="col-md-4">
                <p>
                  <strong>Telefono Cliente:</strong>
                  <?php echo $pedido->telefono_cli; ?>
                <p>
              </div>

            </div>
            <div class="row">
              <div class="col-md-4">
                <p>
                  <strong>Sucursal:</strong>
                  <?php echo $pedido->nombre_suc; ?>
                <p>
              </div>
              <div class="col-md-4">
                <p>
                  <strong>Ciudad:</strong>
                  <?php echo $pedido->ciudad_suc; ?>
                <p>
              </div>
              <div class="col-md-4">
                <p>
                  <strong>Telefono:</strong>
                  <?php echo $pedido->telefono_suc; ?>
                <p>
              </div>

            </div>
            <div class="row">
              <div class="col-md-12">
                <p>
                  <strong>Descripción del pedido:</strong>
                  <?php echo $pedido->descripcion_ped; ?>
                <p>
              </div>


            </div>


          </div>
          <div class="card-block wow fadeInUp" data-wow-delay="0.2s">
            <div id="map" class="set-map" style="height:500px; width:100%;">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br>
<br>
<!-- Service End -->

<script>
  function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 3,
      center: { lat: -0.07620065339336546, lng: -78.50383493559258 }  // Centro de mapa predeterminado
    });


    var directionsService = new google.maps.DirectionsService();
    var directionsRenderer = new google.maps.DirectionsRenderer();
    directionsRenderer.setMap(map);

    var origen = new google.maps.LatLng(<?php echo $pedido->latitud_suc; ?>, <?php echo $pedido->longitud_suc; ?>);  // Punto A
    var destino = new google.maps.LatLng(<?php echo $pedido->latitud_destino_ped; ?>, <?php echo $pedido->longitud_destino_ped; ?>);  // Punto B

    var request = {
      origin: origen,
      destination: destino,
      travelMode: google.maps.TravelMode.DRIVING
    };

    directionsService.route(request, function (result, status) {
      if (status === 'OK') {
        directionsRenderer.setDirections(result);

        // Agregar evento de clic al marcador de punto A
        var markerA = new google.maps.Marker({
          position: origen,
          map: map,
          title: 'Origen',
        });

        // Agregar evento de clic al marcador de punto B
        var markerB = new google.maps.Marker({
          position: destino,
          map: map,
          title: 'Destino',

        });
      } else {
        // Si falla la ruta de carro, traza la ruta de avión
        var flightPlanCoordinates = [origen, destino];

        var elevator = new google.maps.ElevationService();
        elevator.getElevationAlongPath({
          path: flightPlanCoordinates,
          samples: 100
        }, function (results, status) {
          if (status === 'OK') {
            var elevations = results.map(function (result) {
              return result.elevation;
            });

            var path = flightPlanCoordinates.map(function (coords, index) {
              return new google.maps.LatLng(coords.lat(), coords.lng(), elevations[index]);
            });

            var flightPath = new google.maps.Polyline({
              path: path,
              geodesic: true,
              strokeColor: '#FF0000',
              strokeOpacity: 1.0,
              strokeWeight: 3
            });

            flightPath.setMap(map);


            var markerA = new google.maps.Marker({
              position: origen,
              map: map,
              title: 'Origen',
              icon: "<?php echo base_url(); ?>/assets/img/avion.png"
            });


            var markerB = new google.maps.Marker({
              position: destino,
              map: map,
              title: 'Destino',

            });

          } else {
            window.alert('Error al trazar la ruta, no se encontro el lugar');
          }
        });
      }
    });


  }
</script>
</body>

</html>
