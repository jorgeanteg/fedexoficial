<!-- Page Header Start -->
<div class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>Busca tu Pedido</h2>
      </div>
    </div>
  </div>
</div>
<div class="service">
  <div class="container">
    <div class="section-header text-center">
      <h2>Busca los Detalles de tu Pedido</h2>
    </div>
    <div class="row align-items-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header wow fadeInUp" data-wow-delay="0.1s">
            <br>
            <h5>Ingresa el código de tu pedido para realizar tu busqueda</h5>
            <br>
            <span>
              <div class="col-md-12">
                <form id="buscar" method="post" action="<?php echo site_url(); ?>/pedidos/buscarpedidocli">
                  <input type="text" class="form-control" id="query" name="query">
                  <br>
                  <button type="submit" name="button" class="btn btn-primary" id="buscar" value="BUSCAR">BUSCAR</button>
                </form>
              </div>
            </span>
            <div class="card-block table-border-style">
              <div class="table-responsive">
                <?php if ($results): ?>
                  <br><br>
                  <h2>Resultado de la busqueda</h2>
                  <table class="table table-striped table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>CÓDIGO DEL ENVÍO</th>
                        <th>NOMBRE</th>
                        <th>DESCRIPCIÓN DEL ENVÍO</th>
                        <th>FECHA DEL ENVÍO</th>
                        <th>DESTINO</th>
                        <th>ACCIONES</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php foreach ($results as $result): ?>
                        <tr>
                          <td>
                            <?php echo $result->codigo_ped ?>
                          </td>
                          <td>
                            <?php echo $result->nombre_ped ?>
                          </td>
                          <td>
                            <?php echo $result->descripcion_ped ?>
                          </td>
                          <td>
                            <?php echo $result->fecha_ped ?>
                          </td>
                          <td>
                            <?php echo $result->pais_destino_ped ?>
                          </td>
                          <td class="text-center">
                            <a href="<?php echo site_url(); ?>/pedidos/ver_pedidoCli/<?php echo $result->id_ped; ?>"
                              title="Detalles del Envío">
                              <i class="bi bi-geo-alt-fill"></i>
                            </a>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                  <br>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>