<div class="pcoded-inner-content">
	<div class="main-body">
		<div class="page-wrapper">
			<h1>Geolocalización General de los Pedidos</h1>
			<!-- Page body start -->
			<div class="page-body">
				<div class="row">
					<div class="col-md-12">

						<div class="card">
							<div class="card-header">
								<h5>Destino de los Pedidos</h5>
								<span>Se presentan las ubicaciones de los destinos de todos los Pedidos del mundo</span>
							</div>
							<div class="card-block">
								<div id="mapaDestino" style="height:500px; width:100%;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
S
		<script type="text/javascript">
			function initMap(){
				var centro=new google.maps.LatLng(-0.9177264322536244, -78.63301799183898);
				var mapaPedidos=new google.maps.Map(document.getElementById('mapaDestino'),
					{
						center:centro,
						zoom:2,
						mapTypeId:google.maps.MapTypeId.HYBRID
					}
				);
				<?php if($pedidos): ?>
					<?php foreach ($pedidos as $lugarTemporal): ?>
					var coordenadTemporal=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_destino_ped; ?>, <?php echo $lugarTemporal->longitud_destino_ped; ?>);
					var marcador=new google.maps.Marker(
						{
							position:coordenadTemporal,
							title:"<?php echo $lugarTemporal->nombre_ped; ?>",
							map:mapaPedidos,
							icon: "<?php echo base_url(); ?>/assets/img/destino.png"
						}
					);
					<?php endforeach; ?>
				<?php endif; ?>
			}//cierre de la funcion initMap
		</script>
