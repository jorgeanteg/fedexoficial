<div class="pcoded-inner-content">
  <div class="main-body">
    <div class="page-wrapper">
      <h1>NUEVO PEDIDO</h1>
      <div class="page-body">
        <div class="card">
          <div class="card-header">
            <h5>Datos de los pedidos</h5>
            <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
          </div>
          <div class="card-block">
            <form class="" id="frm_nuevo_pedido" action="<?php echo site_url(); ?>/pedidos/guardarpedido" method="post">
              <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <label for="">Nombre del Envío</label>
                    <span class="obligatorio">*</span>
                    <br>
                    <input type="text" placeholder="Ingrese el nombre del pedido" class="form-control" name="nombre_ped"
                      value="" id="nombre_ped">
                  </div>
                  <div class="col-md-6">
                    <label for="">Descripción del envío</label>
                    <span class="obligatorio">*</span>
                    <br>
                    <input type="text" placeholder="Ingrese la descripción del pedido" class="form-control"
                      name="descripcion_ped" value="" id="descripcion_ped">
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-6">
                    <label for="">Cliente</label>
                    <span class="obligatorio">*</span>
                    <br>
                    <select name="id_fk_cli" id="id_fk_cli" class="form-select">
                      <?php foreach ($cliente as $cliente) { ?>
                        <option disabled selected hidden>Seleccione el cliente que va a enviar el producto</option>
                        <option value="<?php echo $cliente['id_cli']; ?>"><?php echo $cliente['apellido_cli'] ?>   <?php echo $cliente['nombre_cli'] ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-md-6">
                    <label for="">Fecha de Envío</label>
                    <span class="obligatorio">*</span>
                    <br>
                    <input type="date" placeholder="Ingrese la fecha de envio del pedido" class="form-control"
                      name="fecha_ped" value="" id="fecha_ped">
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="text-center">
                    <label for=""><strong>Origen del Envío</strong></label>
                  </div>
                  <div class="col-md-12">
                    <label for="">Sucursal</label>
                    <span class="obligatorio">*</span>
                    <br>
                    <select name="id_fk_suc" id="id_fk_suc" class="form-select">
                      <?php foreach ($sucursal as $sucursal) { ?>
                        <option disabled selected hidden>Seleccione la sucursal desde donde se va a enviar el producto
                        </option>
                        <option value="<?php echo $sucursal['id_suc']; ?>"><?php echo $sucursal['nombre_suc']; ?> <?php echo $sucursal['pais_suc']; ?>/<?php echo $sucursal['ciudad_suc']; ?></option>
                      <?php } ?>
                    </select>
                    <br>
                  </div>
                </div>
                <div class="row">
                  <div class="text-center">
                    <label for=""><strong>Destino del Envío</strong></label>
                  </div>
                  <div class="col-md-6">
                    <label for="">País Destino</label>
                    <span class="obligatorio">*</span>
                    <input type="text" placeholder="Seleccione el país destino" class="form-control"
                      readonly name="pais_destino_ped" value="" id="pais_destino_ped">
                  </div>
                  <div class="col-md-6">
                    <label for="">Ciudad Destino</label>
                    <span class="obligatorio">*</span>
                    <input type="text" placeholder="Seleccione la ciudad destino" class="form-control"
                      readonly name="ciudad_destino_ped" value="" id="ciudad_destino_ped">
                  </div>
                  <div class="col-md-6">
                    <label for="">Latitud de destino del Envío</label>
                    <span class="obligatorio">*</span>
                    <input type="text" placeholder="Seleccione la latitud de destino del envío" class="form-control"
                      readonly name="latitud_destino_ped" value="" id="latitud_destino_ped">
                  </div>
                  <div class="col-md-6">
                    <label for="">Longitud de destino del Envío</label>
                    <span class="obligatorio">*</span>
                    <input type="text" placeholder="Seleccione la longitud de destino del envío" class="form-control"
                      readonly name="longitud_destino_ped" value="" id="longitud_destino_ped">
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <div id="mapaUbicacion" style="height:350px; width:100%; border:2px solid black;"></div>
                  </div>
                </div>


                <br>
                <div class="row">
                  <br><br>
                  <div class="col-md-12 text-center">
                    <button type="submit" name="button" class="btn btn-primary">Guardar</button>
                    &nbsp
                    <a href="<?php echo site_url(); ?>/pedidos/listadopedido" class="btn btn-danger">Cancelar</a>
                  </div>
                </div>
            </form>
            <script type="text/javascript">
              $("#frm_nuevo_pedido").validate({
                rules: {
                  nombre_ped: {
                    required: true,
                    minlength: 2,
                    maxlength: 100,
                    letras: true
                  },
                  descripcion_ped: {
                    required: true,
                    minlength: 3,
                    maxlength: 500,
                  },
                  fecha_ped: {
                    required: true
                  },
                  pais_destino_ped: {
                    required: true
                  },
                  latitud_destino_ped: {
                    required: true
                  },
                  longitud_destino_ped: {
                    required: true
                  },
                  codigo_ped: {
                    required: true,
                    minlength: 2,
                    maxlength: 20,
                  },
                  id_fk_cli: {
                    required: true,
                  },
                  id_fk_suc: {
                    required: true,
                  },
                },
                messages: {
                  nombre_ped: {
                    required: "Por favor ingrese el nombre del pedido",
                    minlength: "El nombre del pedido debe tener al menos 2 caracteres",
                    maxlength: "El nombre del pedido excede el número de caracteres permitidos",
                  },
                  descripcion_ped: {
                    required: "Por favor ingrese la descripción del pedido",
                    minlength: "La descripción del pedido debe tener al menos 3 caracteres",
                    maxlength: "La descripción del pedido excede el número de caracteres permitidos",
                  },
                  fecha_ped: {
                    required: "Por favor seleccione obligatoriamente la fecha de recepción del pedido"
                  },
                  pais_destino_ped: {
                    required: "Por favor debe seleccionar obligatoriamente el país de destino del pedido"
                  },
                  latitud_destino_ped: {
                    required: "Por favor despliegue el marcador en el mapa sobre la ubicación que necesita"
                  },
                  longitud_destino_ped: {
                    required: "Por favor despliegue el marcador en el mapa sobre la ubicación que necesita"
                  },
                  codigo_ped: {
                    required: "Por favor debe asignar obligatoriamente un código al pedido",
                    minlength: "El código del pedido debe tener al menos 2 caracteres",
                    maxlength: "El código del pedido excede el número de caracteres permitidos",
                  },
                  id_fk_cli: {
                    required: "Por favor seleccione el cliente que va a enviar el pedido"
                  },
                  id_fk_suc: {
                    required: "Por favor seleccione la sucursal desde donde se va a enviar el pedido"
                  },
                }
              });
            </script>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


<script type="text/javascript">
  function initMap() {
    var centro = new google.maps.LatLng(-1.6654563910937459, -78.66144701362455);
    var mapa1 = new google.maps.Map(
      document.getElementById('mapaUbicacion'),
      {
        center: centro,
        zoom: 2,
        mapTypeId: google.maps.MapTypeId.HYBRID
      }
    );

    var geocoder = new google.maps.Geocoder();
    var marcador = new google.maps.Marker(
      {
        position: centro,
        map: mapa1,
        title: "Seleccione la dirección",
        icon: "<?php echo base_url() ?>/assets/img/icon2.png",
        draggable: true
      }
    );
    google.maps.event.addListener(marcador, 'dragend', function () {
      var latLng = this.getPosition();

      // Realizar la solicitud de geocodificación inversa
      geocoder.geocode({ 'location': latLng }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            var addressComponents = results[0].address_components;
            var country, city;
            for (var i = 0; i < addressComponents.length; i++) {
              var types = addressComponents[i].types;
              if (types.includes('country')) {
                country = addressComponents[i].long_name;
              } else if (types.includes('locality') || types.includes('administrative_area_level_1')) {
                city = addressComponents[i].long_name;
              }
            }
            document.getElementById('pais_destino_ped').value = country;
            document.getElementById('ciudad_destino_ped').value = city;
          }
        }
      });

      document.getElementById('latitud_destino_ped').value = latLng.lat();
      document.getElementById('longitud_destino_ped').value = latLng.lng();
    });
  }//cierre de la funcion initMap
</script>