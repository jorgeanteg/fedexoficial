<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <h1>LISTADO DE CLIENTES</h1>
            <div class="page-body">
                <div class="card">
                    <div class="card-header">
                        <h5>Listado de los Clientes</h5>
                        <span>
                            <div class="col-md-4 ">
                                <a href="<?php echo site_url(); ?>/clientes/nuevo" class="btn btn-primary">
                                    <i class="ti-plus"></i>
                                    Nuevo Cliente
                                </a>
                            </div>
                        </span>
                        <div class="card-header-right">
                            <ul class="list-unstyled card-option">
                                <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                <li><i class="fa fa-window-maximize full-card"></i></li>
                                <li><i class="fa fa-minus minimize-card"></i></li>
                                <li><i class="fa fa-refresh reload-card"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-block table-border-style">
                        <div class="table-responsive">
                            <?php if ($clientes): ?>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>CÉDULA</th>
                                            <th>APELLIDOS</th>
                                            <th>NOMBRES</th>
                                            <th>TELÉFONO</th>
                                            <th>PAÍS</th>
                                            <th>ACCIONES</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <?php foreach ($clientes as $filaTemporal): ?>
                                            <tr>
                                                <td>
                                                    <?php echo $filaTemporal->id_cli; ?>
                                                </td>
                                                <td>
                                                    <?php echo $filaTemporal->cedula_cli; ?>
                                                </td>
                                                <td>
                                                    <?php echo $filaTemporal->apellido_cli; ?>
                                                </td>
                                                <td>
                                                    <?php echo $filaTemporal->nombre_cli; ?>
                                                </td>
                                                <td>
                                                    <?php echo $filaTemporal->telefono_cli; ?>
                                                </td>
                                                <td>
                                                    <?php echo $filaTemporal->pais_cli; ?>
                                                </td>

                                                <td class="text-center">
                                                  <a href="<?php echo site_url(); ?>/clientes/editarcliente/<?php echo $filaTemporal->id_cli ?>" title="Editar Cliente" style="color:blue;">
                                                      <i class="bi bi-pencil-square"></i>
                                                  </a>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="<?php echo site_url(); ?>/clientes/eliminar/<?php echo $filaTemporal->id_cli; ?>"
                                                        title="Eliminar Clientes"
                                                        onclick="return confirm('¿Está seguro de eliminar este registro?');"
                                                        style="color:red;">
                                                        <i class="bi bi-trash-fill"></i>
                                                    </a>

                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>


                                </table>

                            <?php else: ?>
                                <h1>No hay Clientes</h1>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
