<div class="pcoded-inner-content">
	<div class="main-body">
		<div class="page-wrapper">
			<h1>Geolocalización General de los Clientes</h1>
			<!-- Page body start -->
			<div class="page-body">
				<div class="row">
					<div class="col-md-12">

						<div class="card">
							<div class="card-header">
								<h5>Sucursales</h5>
								<span>Se presenta las ubicaciones de los Clientes de todo el mundo</span>
							</div>
							<div class="card-block">
								<div id="mapaCliente" class="set-map" style="height:500px; width:100%;">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function initMap() {
		//Crear el punto central del mapa
		var centro = new google.maps.LatLng(-0.9177264322536244, -78.63301799183898);

		//Creando mapa
		//Tipos de mapa
		//SATELLITE
		//TERRAIN
		var mapaCliente = new google.maps.Map(document.getElementById('mapaCliente'),
			{ center: centro, zoom: 2, mapTypeId: google.maps.MapTypeId.HYBRID });

		//Marcador


		//$lugares viene desde el controlador
		<?php if ($clientes): ?>
			<?php foreach ($clientes as $lugarTemporal): ?>

				var coordenadaTemporal = new google.maps.LatLng(<?php echo $lugarTemporal->latitud_cli; ?>, <?php echo $lugarTemporal->longitud_cli; ?>);

				var marcadorTemporal = "<?php echo $lugarTemporal->nombre_cli; ?> <?php echo $lugarTemporal->apellido_cli; ?>";

				var marcador = new google.maps.Marker(
					{
						position: coordenadaTemporal,
						title: marcadorTemporal,
						map: mapaCliente,
						icon: "<?php echo base_url(); ?>/assets/img/cliente1.png"

					});
			<?php endforeach; ?>

		<?php endif; ?>


	}

</script>
