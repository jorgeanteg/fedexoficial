



<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <h1>NUEVO CLIENTE</h1>
            <div class="page-body">
                <div class="card">
                    <div class="card-header">
                        <h5>Datos de los clientes</h5>
                        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
                    </div>
                    <div class="card-block">

                        <form id="frm_nuevo_cliente" class="" action="<?php echo site_url(); ?>/clientes/guardar" method="post">
                            <div class="row">

                                <div class="col-md-6">
                                    <label for="">Cédula:</label>
                                    <span class="obligatorio" >*</span>
                                    <br>
                                    <input type="number" placeholder="Ingrese la cédula" class="form-control"
                                        name="cedula_cli" value="" id="cedula_cli">
                                </div>
                                <div class="col-md-6">
                                    <label for="">Apellidos:</label>
                                    <span class="obligatorio" >*</span>
                                    <br>
                                    <input type="text" placeholder="Ingrese sus apellidos" class="form-control"
                                        name="apellido_cli" value="" id="apellido_cli">
                                </div>
                            </div>
                            <br>
                            <div class="row">

                                <div class="col-md-6">
                                    <label for="">Nombres:</label>
                                    <span class="obligatorio" >*</span>
                                    <br>
                                    <input type="text" placeholder="Ingrese los nombres" class="form-control"
                                        name="nombre_cli" value="" id="nombre_cli">
                                </div>
                                <div class="col-md-6">
                                    <label for="">Teléfono:</label>
                                    <span class="obligatorio" >*</span>
                                    <br>
                                    <input type="text" placeholder="Ingrese el titulo" class="form-control"
                                        name="telefono_cli" value="" id="telefono_cli">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="">País:</label>
                                    <span class="obligatorio" >*</span>
                                    <br>
                                    <select class="form-select" type="text" name="pais_cli" id="pais_cli">
                                        <option selected disabled>Selecione su País</option>
                                        <option value="Argentina">Argentina</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Bolivia">Bolivia</option>
                                        <option value="Brasil">Brasil</option>
                                        <option value="Canadá">Canadá</option>
                                        <option value="Colombia">Colombia</option>
                                        <option value="China">China</option>
                                        <option value="Corea del Sur">Corea del Sur</option>
                                        <option value="Corea del Norte">Corea del Norte</option>
                                        <option value="Ecuador">Ecuador</option>
                                        <option value="Estados Unidos">Estados Unidos</option>
                                        <option value="España">España</option>
                                        <option value="Francia">Francia</option>
                                        <option value="Japón">Japón</option>
                                        <option value="México">México</option>
                                        <option value="Perú">Perú</option>
                                        <option value="Rusia">Rusia</option>
                                        <option value="Ucrania">Ucrania</option>
                                        <option value="Turquía">Turquía</option>
                                        <option value="Otros">Otros</option>

                                    </select>
                                </div>



                            </div>

                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for=""><strong>Dirección</strong></label>
                                    <span class="obligatorio" >*</span>
                                </div>

                                <br>
                                <div class="col-md-6">
                                    <label for="">Latitud:</label>
                                    <br>
                                    <input type="number" placeholder="Ingrese la direccion" class="form-control"
                                        readonly name="latitud_cli" value="" id="latitud_cli">
                                </div>
                                <div class="col-md-6">
                                    <label for="">Longitud:</label>
                                    <br>
                                    <input type="number" placeholder="Ingrese la direccion" class="form-control"
                                        readonly name="longitud_cli" value="" id="longitud_cli">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <br>
                                    <div id="mapaUbicacion" style="height:350px; width:100%; border:2px solid black;">

                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" name="button" class="btn btn-primary">
                                        Guardar
                                    </button>
                                    &nbsp;
                                    <a href="<?php echo site_url(); ?>/clientes/listado" class="btn btn-danger">
                                        Cancelar
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

<script type="text/javascript">
    function initMap() {
        //Crear el punto central del mapa
        var centro = new google.maps.LatLng(-1.6654563910937459, -78.66144701362455);

        //Creando mapa
        //Tipos de mapa
        //SATELLITE
        //TERRAIN
        var mapa1 = new google.maps.Map(document.getElementById('mapaUbicacion'),
            { center: centro, zoom: 2, mapTypeId: google.maps.MapTypeId.HYBRID });


        var marcador = new google.maps.Marker(
            {
                position: centro,
                map: mapa1,
                title: "Seleccione la dirección",
                icon: "<?php echo base_url(); ?>/assets/img/icon2.png",
                draggable: true

            });

        google.maps.event.addListener(marcador, 'dragend', function () {
            // alert("Se termino el drag");
            document.getElementById('latitud_cli').value = this.getPosition().lat();
            document.getElementById('longitud_cli').value = this.getPosition().lng();
        });
    }
</script>


<script type="text/javascript">
    $("#frm_nuevo_cliente").validate({
        rules: {
            cedula_cli: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            apellido_cli: {
                required: true,
                minlength: 3,
                maxlength: 150,
                letras: true
            },
            nombre_cli: {
                required: true,
                minlength: 2,
                maxlength: 150,
                letras: true
            },
            telefono_cli: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            pais_cli: {
                required: true,
            },
            latitud_cli: {
                required: true,
            },
            longitud_cli: {
                required: true,
            },

        },
        messages: {
            cedula_cli: {
                required: "Por favor ingrese su número de cédula",
                minlength: "Cédula incorrecta, ingrese 10 digitos",
                maxlength: "Cédula incorrecta, ingrese 10 digitos",
                digits: "Este campo solo acepta número",
                number: "Este campo solo acepta número"
            },
            apellido_cli: {
                required: "Por favor ingrese su apellido",
                minlength: "El apellido debe tener al menos 3 caracteres",
                maxlength: "Apellido incorrecto",

            },
            nombre_cli: {
                required: "Por favor ingrese su nombre",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "Nombre incorrecto",
            },
            telefono_cli: {
                required: "Por favor ingrese su teléfono",
                minlength: "El teléfono debe tener 10 caracteres",
                maxlength: "Teléfono incorrecto",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",
            },
            pais_cli: {
                required: "Por favor selecione un país",
            },
            latitud_cli: {
                required: "Por favor debe selecionar una latitud con el marcador",
            },
            longitud_cli: {
                required: "Por favor debe selecionar una longitud con el marcador",
            },

        }
    });
</script>