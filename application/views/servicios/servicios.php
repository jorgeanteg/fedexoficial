<!-- Page Header Start -->
<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Nuestros Servicios</h2>
            </div>
            <div class="col-12">
                <a href="<?php echo site_url(); ?>">Inicio</a>
                <a href="<?php echo site_url(); ?>/servicios/servicios">Servicios</a>
            </div>
        </div>
    </div>
</div>
<!-- Page Header End -->


<!-- Service Start -->
<div class="service">
    <div class="container">
        <div class="section-header text-center">
            <p>Nuestro Services</p>
            <h2>Te contamos un poco mas sobre todos nuestros servicios</h2>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                <div class="service-item">
                    <div class="service-img">
                        <img src="<?php echo base_url(); ?>/assets/img/documentos.jpg" alt="Image">
                        <div class="service-overlay">
                            <p>
                                Nos encargamos que envies tus documentos de forma segura, rápida y eficiente.
                            </p>
                        </div>
                    </div>
                    <div class="service-text">
                        <h3>Documentos</h3>
                        <a class="btn" href="<?php echo base_url(); ?>/assets/img/documentos.jpg" data-lightbox="service">+</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                <div class="service-item">
                    <div class="service-img">
                        <img src="<?php echo base_url(); ?>/assets/img/mercancias.jpg" style="height:247px; width:100%" alt="Image">
                        <div class="service-overlay">
                            <p>
                                Tu mercancia viaja segura hasta su destino, te ofrecemos el sercicio de entrega ágil y entrega normal.
                            </p>
                        </div>
                    </div>
                    <div class="service-text">
                        <h3>Mercancías</h3>
                        <a class="btn" href="<?php echo base_url(); ?>/assets/img/mercancias.jpg" data-lightbox="service">+</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                <div class="service-item">
                    <div class="service-img">
                        <img src="<?php echo base_url(); ?>/assets/img/farmacia.jpg" alt="Image">
                        <div class="service-overlay">
                            <p>
                                Realizamos el envío de medicamentos a cualquier parte del mundo que desees.
                            </p>
                        </div>
                    </div>
                    <div class="service-text">
                        <h3>ServiFarma</h3>
                        <a class="btn" href="<?php echo base_url(); ?>/assets/img/farmacia.jpg" data-lightbox="service">+</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                <div class="service-item">
                    <div class="service-img">
                        <img src="<?php echo base_url(); ?>/assets/img/embalaje.jpg" alt="Image">
                        <div class="service-overlay">
                            <p>
                                Contamos con el mejor servicio de empaque y embalaje, para que tus envíos lleguen correcta y adecuadamente a su destino.
                            </p>
                        </div>
                    </div>
                    <div class="service-text">
                        <h3>Empaque y Embalaje</h3>
                        <a class="btn" href="<?php echo base_url(); ?>/assets/img/embalaje.jpg" data-lightbox="service">+</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                <div class="service-item">
                    <div class="service-img">
                        <img src="<?php echo base_url(); ?>/assets/img/almacen.jpg" style="height:304px; width:100%" alt="Image">
                        <div class="service-overlay">
                            <p>
                                Contamos con las mejores instalaciones para almacenar tus pedidos hasta su envío.
                            </p>
                        </div>
                    </div>
                    <div class="service-text">
                        <h3>Almacenamiento</h3>
                        <a class="btn" href="<?php echo base_url(); ?>/assets/img/almacen.jpg" data-lightbox="service">+</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Service End -->
