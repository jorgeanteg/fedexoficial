<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <h1>LISTADO DE SUCURSALES</h1>
            <div class="page-body">
                <div class="card">
                    <div class="card-header">
                        <h5>Listado de los Sucursales</h5>
                        <span>
                            <div class="col-md-4 ">
                                <a href="<?php echo site_url(); ?>/sucursales/nuevo" class="btn btn-primary">
                                    <i class="ti-plus"></i>
                                    Nuevo Sucursal
                                </a>
                            </div>
                        </span>
                        <div class="card-header-right">
                            <ul class="list-unstyled card-option">
                                <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                <li><i class="fa fa-window-maximize full-card"></i></li>
                                <li><i class="fa fa-minus minimize-card"></i></li>
                                <li><i class="fa fa-refresh reload-card"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-block table-border-style">
                        <div class="table-responsive">
                            <?php if ($sucursales): ?>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>NOMBRE</th>
                                            <th>TELÉFONO</th>
                                            <th>PAÍS</th>
                                            <th>PROVINCIA O CIUDAD</th>
                                            <th>ACCIONES</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <?php foreach ($sucursales as $filaTemporal): ?>
                                            <tr>
                                                <td>
                                                    <?php echo $filaTemporal->id_suc; ?>
                                                </td>

                                                <td>
                                                    <?php echo $filaTemporal->nombre_suc; ?>
                                                </td>
                                                <td>
                                                    <?php echo $filaTemporal->telefono_suc; ?>
                                                </td>
                                                <td>
                                                    <?php echo $filaTemporal->pais_suc; ?>
                                                </td>
                                                <td>
                                                    <?php echo $filaTemporal->ciudad_suc; ?>
                                                </td>

                                                <td class="text-center">
                                                  <a href="<?php echo site_url(); ?>/sucursales/editarsucursal/<?php echo $filaTemporal->id_suc ?>" title="Editar Sucursal" style="color:blue;">
                                                      <i class="bi bi-pencil-square"></i>
                                                  </a>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="<?php echo site_url(); ?>/sucursales/eliminar/<?php echo $filaTemporal->id_suc; ?>"
                                                        title="Eliminar Sucursal"
                                                        onclick="return confirm('¿Está seguro de eliminar este registro?');"
                                                        style="color:red;">
                                                        <i class="bi bi-trash-fill"></i>
                                                    </a>

                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>


                                </table>

                            <?php else: ?>
                                <h1>No hay Sucursales</h1>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
