<style>
    select option {
        color: #495057;
    }
</style>



<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <h1>ACTUALIZAR SUCURSAL</h1>
            <div class="page-body">
                <div class="card">
                    <div class="card-header">
                        <h5>Editar los Datos de la Sucursal</h5>
                        <!--<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>-->
                    </div>
                    <div class="card-block">

                        <form class="" id="frm_nuevo_sucursal" action="<?php echo site_url(); ?>/sucursales/procesarActualizacion" method="post">

                            <div class="row">
                              <input type="hidden" name="id_suc" id="id_suc" value="<?php echo $sucursalEditar->id_suc; ?>">
                                <div class="col-md-6">
                                    <label for="">Nombres:</label>
                                    <span class="obligatorio" >*</span>
                                    <br>
                                    <input type="text" placeholder="Ingrese los nombres" class="form-control"
                                        name="nombre_suc" value="<?php echo $sucursalEditar->nombre_suc; ?>" id="nombre_suc">
                                </div>
                                <div class="col-md-6">
                                    <label for="">Teléfono:</label>
                                    <span class="obligatorio" >*</span>
                                    <br>
                                    <input type="text" placeholder="Ingrese el titulo" class="form-control"
                                        name="telefono_suc" value="<?php echo $sucursalEditar->telefono_suc; ?>" id="telefono_suc">
                                </div>
                            </div>
                            <br>


                            <div class="row">
                                <div class="col-md-12">
                                    <label for=""><strong>Dirección</strong></label>
                                    <span class="obligatorio" >*</span>
                                </div>

                                <br>
                                <div class="col-md-6">
                                    <label for="">País:</label>
                                    <br>
                                    <input type="text" placeholder="País" class="form-control"
                                        readonly name="pais_suc" value="<?php echo $sucursalEditar->pais_suc; ?>" id="pais_suc">
                                </div>
                                <div class="col-md-6">
                                    <label for="">Ciudad:</label>
                                    <br>
                                    <input type="text" placeholder="Ciudad" class="form-control"
                                        readonly name="ciudad_suc" value="<?php echo $sucursalEditar->ciudad_suc; ?>" id="ciudad_suc">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <br>
                                <div class="col-md-6">
                                    <label for="">Latitud:</label>
                                    <br>
                                    <input type="number" placeholder="Ingrese la direccion" class="form-control"
                                        readonly name="latitud_suc" value="<?php echo $sucursalEditar->latitud_suc; ?>" id="latitud_suc">
                                </div>
                                <div class="col-md-6">
                                    <label for="">Longitud:</label>
                                    <br>
                                    <input type="number" placeholder="Ingrese la direccion" class="form-control"
                                        readonly name="longitud_suc" value="<?php echo $sucursalEditar->longitud_suc; ?>" id="longitud_suc">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <br>
                                    <div id="mapaUbicacion" style="height:350px; width:100%; border:2px solid black;">

                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" name="button" class="btn btn-primary">
                                        Actualizar Registro
                                    </button>
                                    &nbsp;
                                    <a href="<?php echo site_url(); ?>/sucursales/listado" class="btn btn-danger">
                                        Cancelar
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

<script type="text/javascript">
  function initMap() {
      var centro = new google.maps.LatLng(<?php echo $sucursalEditar->latitud_suc; ?>, <?php echo $sucursalEditar->longitud_suc; ?>);
      var mapa1 = new google.maps.Map(document.getElementById('mapaUbicacion'), {
          center: centro,
          zoom: 7,
          mapTypeId: google.maps.MapTypeId.HYBRID
      });

      var geocoder = new google.maps.Geocoder();
      var marcador = new google.maps.Marker({
          position: centro,
          map: mapa1,
          title: "Seleccione la dirección",
          icon: "<?php echo base_url(); ?>/assets/img/sucursal.png",
          draggable: true
      });

      google.maps.event.addListener(marcador, 'dragend', function() {
          var latLng = this.getPosition();

          // Realizar la solicitud de geocodificación inversa
          geocoder.geocode({ 'location': latLng }, function(results, status) {
              if (status === google.maps.GeocoderStatus.OK) {
                  if (results[0]) {
                      var addressComponents = results[0].address_components;
                      var country, city;
                      for (var i = 0; i < addressComponents.length; i++) {
                          var types = addressComponents[i].types;
                          if (types.includes('country')) {
                              country = addressComponents[i].long_name;
                          } else if (types.includes('locality') || types.includes('administrative_area_level_1')) {
                              city = addressComponents[i].long_name;
                          }
                      }
                      document.getElementById('pais_suc').value = country;
                      document.getElementById('ciudad_suc').value = city;
                  }
              }
          });

          document.getElementById('latitud_suc').value = latLng.lat();
          document.getElementById('longitud_suc').value = latLng.lng();
      });
  }
</script>

<script type="text/javascript">
    $("#frm_nuevo_sucursal").validate({
        rules: {
            nombre_suc: {
                required: true,
                minlength: 2,
                maxlength: 150,
            },
            telefono_suc: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            pais_suc: {
                required: true,
            },
            latitud_suc: {
                required: true,
            },
            longitud_suc: {
                required: true,
            },

        },
        messages: {
            nombre_suc: {
                required: "Por favor ingrese el nombre de la sucursal",
                minlength: "El nombre debe tener al menos 2 caracteres",
                maxlength: "Nombre incorrecto",
            },
            telefono_suc: {
                required: "Por favor ingrese el teléfono",
                minlength: "El teléfono debe tener 10 caracteres",
                maxlength: "Teléfono incorrecto",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",
            },
            pais_suc: {
                required: "Por favor selecione un país",
            },
            latitud_suc: {
                required: "Por favor debe selecionar una latitud con el marcador",
            },
            longitud_suc: {
                required: "Por favor debe selecionar una longitud con el marcador",
            },

        }
    });
</script>
