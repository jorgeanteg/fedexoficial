<style>

</style>

<!-- Page Header Start -->
<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Sobre Nosotros</h2>
            </div>
            <div class="col-12">
                <a href="">Misión</a>
                <a href="">Visión</a>
            </div>
        </div>
    </div>
</div>
<!-- Page Header End -->
<!-- About Start -->
<div class="about wow fadeInUp" data-wow-delay="0.1s">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 col-md-6">
                <div class="about-img img-fluid rounded d-block m-l-none">
                    <img src="<?php echo base_url(); ?>/assets/img/nosotros.png" alt="Image">
                </div>
            </div>
            <div class="col-lg-7 col-md-6">
                <div class="section-header text-left">
                    <p>Bienvenido a FEDEX</p>
                    <h2>25 Años de Experiencia</h2>
                </div>
                <div class="about-text">
                    <h3>Misión</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec pretium mi. Curabitur
                        facilisis ornare velit non vulputate. Aliquam metus tortor, auctor id gravida condimentum,
                        viverra quis sem.
                    </p>
                    <h3>Visión</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec pretium mi. Curabitur
                        facilisis ornare velit non vulputate. Aliquam metus tortor, auctor id gravida condimentum,
                        viverra quis sem. Curabitur non nisl nec nisi scelerisque maximus. Aenean consectetur convallis
                        porttitor. Aliquam interdum at lacus non blandit.
                    </p>
                    <a class="btn" href="<?php echo site_url(); ?>/sucursales/mapaUser" class="nav-item nav-link">Ubícanos</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About End -->
