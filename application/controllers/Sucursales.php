<?php

class Sucursales extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        //Cargar modelo
        $this->load->model('Sucursal');
    }

    public function listado()
    {

        //data es un nombre cualquiera
        $data['sucursales'] = $this->Sucursal->obtenerTodos();

        $this->load->view('administradores/header');
        // estamos pasando los datos a la vista
        $this->load->view('sucursales/listado', $data);
        $this->load->view('administradores/footer');

    }
    public function nuevo()
    {
        $this->load->view('administradores/header');
        $this->load->view('sucursales/nuevo');
        $this->load->view('administradores/footer');
    }

    public function guardar()
    {
        $datosNuevoSucursal = array(
            "nombre_suc" => $this->input->post('nombre_suc'),
            "telefono_suc" => $this->input->post('telefono_suc'),
            "pais_suc" => $this->input->post('pais_suc'),
            "ciudad_suc" => $this->input->post('ciudad_suc'),
            "latitud_suc" => $this->input->post('latitud_suc'),
            "longitud_suc" => $this->input->post('longitud_suc'),
            );

        //llamamos a insertar

        if ($this->Sucursal->insertar($datosNuevoSucursal)) {
            $this->session->set_flashdata("confirmacion","Sucursal guardado exitosamente");
            
        } else {
            $this->session->set_flashdata("error","Error al guardar su registro, intente nuevamente");
        }
        redirect('sucursales/listado');
    }

    public function eliminar($id_suc)
    {
        if ($this->Sucursal->borrar($id_suc)) {
            $this->session->set_flashdata("confirmacion","Sucursal eliminado exitosamente");
            
            # code...
        } else {
            # code...
            $this->session->set_flashdata("error","Error al eliminar su registro, intente nuevamente");
        }
        redirect('sucursales/listado');
    }

    public function mapa()
    {
        //data es un nombre cualquiera
        $data['sucursales'] = $this->Sucursal->obtenerTodos();

        $this->load->view('administradores/header');
        // estamos pasando los datos a la vista
        $this->load->view('sucursales/mapa', $data);
        $this->load->view('administradores/footer');

    }

    public function mostrarMapa() {
        $data['paises'] = $this->obtener_paises();

        $pais = $this->input->post('pais');
        if (!empty($pais)) {
            $data['sucursales'] = $this->Sucursal->obtener_sucursal_por_pais($pais);
        }

        $this->load->view('administradores/header');
        $this->load->view('sucursales/mostrarMapa', $data);
        $this->load->view('administradores/footer');

    }

    public function obtener_paises() {
        // Aquí puedes implementar la lógica para obtener la lista de países desde la base de datos o cualquier otra fuente de datos.
        // Por simplicidad, asumiremos que tienes un array fijo de países.
        return array('Argentina', 'Australia', 'Bolivia', 'Brasil', 'Canadá', 'Colombia', 'China', 'Corea del Sur', 'Corea del Norte', 'Ecuador', 'Estados Unidos', 'España', 'Francia', 'Japón', 'México', 'Perú', 'Rusia', 'Ucrania', 'Turquía', 'Otros');

    }


    public function mapaUser()
    {
        //data es un nombre cualquiera
        $data['sucursales'] = $this->Sucursal->obtenerTodos();

        $this->load->view('header');
        // estamos pasando los datos a la vista
        $this->load->view('sucursales/mapaUser', $data);
        $this->load->view('footer');

    }

    public function mostrarMapaUser() {
        $data['paises'] = $this->obtener_paises();

        $pais = $this->input->post('pais');
        if (!empty($pais)) {
            $data['sucursales'] = $this->Sucursal->obtener_sucursal_por_pais($pais);
        }

        $this->load->view('header');
        $this->load->view('sucursales/mostrarMapaUser', $data);
        $this->load->view('footer');

    }

    public function editarsucursal($id_suc){
      $data["sucursalEditar"]=$this->Sucursal->obtenerPorId($id_suc);
      $this->load->view('administradores/header');
      $this->load->view('sucursales/editarsucursal',$data);
      $this->load->view('administradores/footer');
    }

    public function procesarActualizacion($value='')
    {
      $datosEditados = array(
          "nombre_suc" => $this->input->post('nombre_suc'),
          "telefono_suc" => $this->input->post('telefono_suc'),
          "pais_suc" => $this->input->post('pais_suc'),
          "ciudad_suc" => $this->input->post('ciudad_suc'),
          "latitud_suc" => $this->input->post('latitud_suc'),
          "longitud_suc" => $this->input->post('longitud_suc'),
      );
      $id_suc=$this->input->post("id_suc");
      if ($this->Sucursal->actualizar($id_suc,$datosEditados)) {
        $this->session->set_flashdata("confirmacion","Datos actualizados exitosamente");
      }else {
        $this->session->set_flashdata("error","Error al actualizar su registro, intente nuevamente");
      }
      redirect("sucursales/listado");
    }

} //Cierre d ela clase<

?>
