<?php

  class Pedidos extends CI_Controller
  {

    function __construct()
    {
      parent::__construct();
      $this->load->model('Pedido');
      $this->load->model('Sucursal');
      $this->load->model('Cliente');
    }

    public function nuevopedido(){
      $query=$this->db->get('sucursal');
      $querycli=$this->db->get('cliente');
      $sucursal = $query->result_array();
      $cliente = $querycli->result_array();
      $this->load->view('administradores/header');
      $this->load->view('pedidos/nuevopedido',array('sucursal'=> $sucursal,'cliente'=> $cliente));
      $this->load->view('administradores/footer');
    }

    public function listadopedido(){
      $data['pedidos']=$this->Pedido->obtenerTodosListaU();
      $this->load->view('administradores/header');
      $this->load->view('pedidos/listadopedido',$data);
      $this->load->view('administradores/footer');
    }
    public function destinopedidos(){
      $data['pedidos']=$this->Pedido->obtenerTodos();
      $this->load->view('administradores/header');
      $this->load->view('pedidos/destinopedidos',$data);
      $this->load->view('administradores/footer');
    }

    public function guardarpedido(){


      $ultimoCodigo = $this->Pedido->obtenerUltimoCodigoPedido();
      $ultimoNumero = 0;

      if ($ultimoCodigo) {

        $ultimoNumero = intval(substr($ultimoCodigo, 3));
      }
      // Incrementa el número del último código de pedido
      $nuevoNumero = $ultimoNumero + 1;

      $nuevoCodigo = 'ped' . str_pad($nuevoNumero, 3, '0', STR_PAD_LEFT);

    $datosNuevoPedido=array(
      "nombre_ped"=>$this->input->post('nombre_ped'),
      "descripcion_ped"=>$this->input->post('descripcion_ped'),
      "fecha_ped"=>$this->input->post('fecha_ped'),
      "pais_destino_ped"=>$this->input->post('pais_destino_ped'),
      "ciudad_destino_ped"=>$this->input->post('ciudad_destino_ped'),
      "latitud_destino_ped"=>$this->input->post('latitud_destino_ped'),
      "longitud_destino_ped"=>$this->input->post('longitud_destino_ped'),
      "codigo_ped"=>$nuevoCodigo,
      "id_fk_cli"=>$this->input->post('id_fk_cli'),
      "id_fk_suc"=>$this->input->post('id_fk_suc'),

    );
    if ($this->Pedido->insertar($datosNuevoPedido)) {
      $this->session->set_flashdata("confirmacion","Pedido guardado exitosamente");
    }else {
      $this->session->set_flashdata("error","Error al guardar su registro, intente nuevamente");
      }
      redirect('pedidos/listadopedido');

    }
    public function buscarpedido()
    {
      $query = $this->input->post('query');
      $data['results']=array();
      if ($query) {
        $data['results'] = $this->Pedido->buscar($query);
      }
      $this->load->view('administradores/header');
      $this->load->view('pedidos/buscarpedido',$data);
      $this->load->view('administradores/footer');
    }

    public function buscarpedidocli()
    {
      $query = $this->input->post('query');
      $data['results']=array();
      if ($query) {
        $data['results'] = $this->Pedido->buscar($query);
      }
      $this->load->view('header');
      $this->load->view('pedidos/buscarpedidocli',$data);
      $this->load->view('footer');
    }
    public function eliminar($id_ped){
      if ($this->Pedido->borrar($id_ped))
      {//invocando el modelo
        $this->session->set_flashdata("confirmacion","Pedido eliminado exitosamente");
        
      }else{
        $this->session->set_flashdata("error","Error al eliminar su registro, intente nuevamente");
      }
      redirect('pedidos/listadopedido');
    }

    public function ver_pedido($id_ped) {

      $data['pedido'] = $this->Pedido->obtenerRutaPedido($id_ped);

      $this->load->view('administradores/header');
      $this->load->view('pedidos/ver_pedido', $data);
      $this->load->view('administradores/footer');
  }

<<<<<<< HEAD
=======

>>>>>>> 4d1725442190623346b03a1cda936e3f053e6928
  public function ver_pedidoCli($id_ped) {

      $data['pedido'] = $this->Pedido->obtenerRutaPedido($id_ped);

      $this->load->view('header');
      $this->load->view('pedidos/ver_pedidoCli', $data);
      $this->load->view('footer');
  }

<<<<<<< HEAD
=======
  public function ver_pedidoadm($id_ped) {

      $data['pedido'] = $this->Pedido->obtenerRutaPedido($id_ped);

      $this->load->view('administradores/header');
      $this->load->view('pedidos/verpedidoadm', $data);
      $this->load->view('administradores/footer');
  }

>>>>>>> 4d1725442190623346b03a1cda936e3f053e6928
  public function editarpedido($id_ped){
    $data["sucursales"]=$this->Sucursal->obtenerTodos();
    $data["clientes"]=$this->Cliente->obtenerTodos();
    $data["pedidoEditar"]=$this->Pedido->obtenerPorId($id_ped);
    $this->load->view('administradores/header');
    $this->load->view('pedidos/editarpedido',$data);
    $this->load->view('administradores/footer');
  }
  public function procesarActualizacion()
  {
    $datosEditados = array(
      "nombre_ped"=>$this->input->post('nombre_ped'),
      "descripcion_ped"=>$this->input->post('descripcion_ped'),
      "fecha_ped"=>$this->input->post('fecha_ped'),
      "pais_destino_ped"=>$this->input->post('pais_destino_ped'),
      "ciudad_destino_ped"=>$this->input->post('ciudad_destino_ped'),
      "latitud_destino_ped"=>$this->input->post('latitud_destino_ped'),
      "longitud_destino_ped"=>$this->input->post('longitud_destino_ped'),
      "id_fk_cli"=>$this->input->post('id_fk_cli'),
      "id_fk_suc"=>$this->input->post('id_fk_suc'),

    );
    $id_ped=$this->input->post("id_ped");
    if ($this->Pedido->actualizar($id_ped,$datosEditados)) {
      $this->session->set_flashdata("confirmacion","Datos actualizados exitosamente");
    }else {
      $this->session->set_flashdata("error","Error al actualizar su registro, intente nuevamente");
    }
    redirect("pedidos/listadopedido");
  }

  }
?>
