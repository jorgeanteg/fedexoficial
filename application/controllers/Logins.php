<?php
class Logins extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Login');
        
    }


    public function login(){
        $this->load->view('header');
        $this->load->view('logins/login');
        $this->load->view('footer');
    }
    
    public function process_login() {
        $this->load->view('header');
        $username = $this->input->post('email_user');
        $password = $this->input->post('password_user');
        
        $user = $this->Login->login($username, $password);

        if ($user) {
            $this->session->set_flashdata("confirmacion","Bienvenido al sistema");
            redirect('administradores/administradores');
        } else {
            // Credenciales inválidas, mostrar mensaje de error
            $data['error'] = 'Credenciales inválidas. Por favor, intenta nuevamente.';
            $this->session->set_flashdata("error","Error al iniciar sesión, intente nuevamente");
            $this->load->view('logins/login', $data);
        }
        $this->load->view('footer');
    }
}
?>
