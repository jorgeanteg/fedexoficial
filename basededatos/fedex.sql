-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-06-2023 a las 04:35:51
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `fedex`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cli` int(11) NOT NULL,
  `cedula_cli` varchar(10) NOT NULL,
  `apellido_cli` varchar(201) NOT NULL,
  `nombre_cli` varchar(200) NOT NULL,
  `telefono_cli` varchar(10) NOT NULL,
  `pais_cli` varchar(100) NOT NULL,
  `latitud_cli` double NOT NULL,
  `longitud_cli` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cli`, `cedula_cli`, `apellido_cli`, `nombre_cli`, `telefono_cli`, `pais_cli`, `latitud_cli`, `longitud_cli`) VALUES
(14, '0550249817', 'Ante', 'Jorge', '0980982540', 'Ecuador', 0.7952209945382345, -79.00202318549955),
(15, '0550249819', 'Vargas', 'Luis', '0980982548', 'Ecuador', -4.4298257544021125, -79.03498216987455),
(18, '0550247512', 'Guanoquiza', 'Edison', '0980982540', 'Ecuador', -1.0284287138106836, -80.68293138862455),
(19, '0550213698', 'Guatemal', 'David', '0980451236', 'Ecuador', -0.9295662064810192, -78.60651537299955),
(20, '0450128963', 'Guatemal', 'Luisa', '0980982540', 'Colombia', 1.2346041003210415, -77.29914232612455),
(21, '0550249814', 'Aguilar', 'Sara', '0992563125', 'Colombia', 10.8374614540683, -74.75031420112455),
(22, '0980984563', 'Ante', 'Wilmer', '0980982540', 'Argentina', -40.710748829297096, -65.74152513862452),
(25, '0550213691', 'Jacho', 'David', '0980451236', 'Argentina', -31.387545823679552, -64.18146654487455),
(26, '0550147896', 'Chicaiza', 'Evelyn', '0992563125', 'Australia', -26.38807967423763, 122.0367951738755),
(27, '0550218523', 'Quisaguano ', 'Angel', '0980982540', 'Australia', -27.446035699668492, 153.01824048637542),
(28, '0236987412', 'Sanchez', 'Bladimir', '0980982548', 'Bolivia', -11.044885841488261, -66.04914232612455),
(29, '0231234568', 'Iza', 'Jorge', '0444123658', 'Bolivia', -21.49002684117734, -64.70881029487455),
(30, '0123691245', 'Sanchez', 'Miguel', '0980982540', 'Brasil', -3.114351572188454, -60.05060716987455),
(31, '0550124785', 'Jacho', 'Jorge', '0992563125', 'Brasil', -18.87109931322883, -41.96711107612455),
(32, '0980147874', 'Sinchiguano', 'Camila', '0980124563', 'Canadá', 50.41121384758088, -73.87140795112455),
(33, '0441478523', 'Umaginga', 'Manuel', '0992563125', 'Canadá', 64.03078595738971, -139.22907396674952),
(34, '0987531579', 'Ante', 'Luis', '0992563125', 'Colombia', 10.923772027826876, -74.81623216987455),
(35, '0550121599', 'Guatemal', 'Jorge', '0980982540', 'Colombia', 1.1906686308735015, -70.20197435737455),
(36, '0981597536', 'Sanchez', 'Jose', '0980124563', 'China', 26.061681664660064, 119.33137584159914),
(37, '0159514275', 'Iza', 'Luisa', '0444123658', 'China', 43.9009410020608, 81.35442212700046),
(38, '0159351459', 'Vargas', 'David', '0980982540', 'Corea del Sur', 34.57339017833231, 126.61260083793792),
(39, '0897894563', 'Llumitaxi', 'Alexander', '0992563125', 'Corea del Sur', 38.63305043026194, 128.00511792778167),
(40, '0147896322', 'Gomez', 'Braulio', '0992563125', 'Corea del Norte', 38.03411821399581, 125.71172193168792),
(41, '0236987896', 'Hinojosa', 'Erika', '0980451236', 'Estados Unidos', 47.5200580392216, -122.34308763862455),
(42, '0147014789', 'Guatemal', 'David', '0980982548', 'Corea del Norte', 41.256213175899674, 129.50749829887542),
(43, '0236987514', 'Iza', 'Miriam', '0980124563', 'Estados Unidos', 35.21317841752909, -80.94660326362455),
(44, '0236987845', 'Vargas', 'Luis', '0980124563', 'España', 37.42588593879611, -5.9759001386245725),
(45, '0258963214', 'Ante', 'David', '0980124563', 'España', 41.396468184951665, 2.1539826738753876),
(46, '0189652369', 'Jacho', 'Dilan', '0980451236', 'Francia', 43.29627971523171, 5.3619904863754275),
(47, '0258964569', 'Perez', 'Lucio', '0992563125', 'Francia', 48.8096498526788, 2.4176545488754275),
(48, '0250369815', 'Vargas', 'Alonso', '0980982540', 'Japón', 33.58153958022238, 130.43034986137542),
(49, '0552147852', 'Torres', 'Mayli', '0980982540', 'Japón', 39.67662682463637, 141.17497876762542),
(50, '0550213645', 'Ante', 'Luisa', '0444123658', 'México', 34.56434354359006, -112.49933763862452),
(51, '0258978789', 'Iza', 'Bladimir', '0980451236', 'México', 19.39805867431022, -99.05207201362455),
(52, '0147896356', 'Gomez', 'David', '0980124563', 'Perú', -13.55644875419288, -71.95978685737455),
(53, '1789654238', 'Ayuquina', 'Brayan', '', 'Perú', -3.476300323334388, -74.76130052924955),
(54, '7896321458', 'Bustillos', 'Melany', '0980124563', 'Rusia', 61.982254774753855, 129.81511548637542),
(55, '0551478798', 'Ester', 'Chavez', '0980982540', 'Rusia', 59.054031902321206, 80.90398267387542),
(56, '0789741236', 'Guatemal', 'David', '0992563125', 'Ucrania', 50.72522524070624, 25.335135017625426),
(57, '0458962578', 'Gomez', 'Luis', '0444123658', 'Ucrania', 48.40284142331082, 35.02507642387547),
(58, '1478523698', 'Llumitaxi', 'Brayan', '0444123658', 'Turquía', 39.727342617935435, 30.542654548875426),
(59, '0258369741', 'Lopez', 'Juan', '0444123658', 'Turquía', 37.91287164575663, 40.232595955125426);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `id_ped` int(11) NOT NULL,
  `nombre_ped` varchar(100) NOT NULL,
  `descripcion_ped` varchar(500) NOT NULL,
  `fecha_ped` date NOT NULL,
  `pais_destino_ped` varchar(150) NOT NULL,
  `ciudad_destino_ped` varchar(200) NOT NULL,
  `latitud_destino_ped` double NOT NULL,
  `longitud_destino_ped` double NOT NULL,
  `codigo_ped` varchar(20) NOT NULL,
  `id_fk_cli` int(11) NOT NULL,
  `id_fk_suc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`id_ped`, `nombre_ped`, `descripcion_ped`, `fecha_ped`, `pais_destino_ped`, `ciudad_destino_ped`, `latitud_destino_ped`, `longitud_destino_ped`, `codigo_ped`, `id_fk_cli`, `id_fk_suc`) VALUES
(10, 'Envío de un celular', 'Celular Samsung Galaxi A10', '2023-06-29', 'Colombia', 'Cundinamarca', 4.448873245412616, -73.97938295722807, 'ped001', 34, 8),
(11, 'Envio de documentos personales', 'Cedula, papeleta de votación y licencia de conducir', '2023-06-22', 'Brasil', 'Amazonas', -3.114351572188454, -59.98468920112455, 'ped002', 35, 17);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

CREATE TABLE `sucursal` (
  `id_suc` int(11) NOT NULL,
  `nombre_suc` varchar(200) NOT NULL,
  `telefono_suc` varchar(10) NOT NULL,
  `pais_suc` varchar(100) NOT NULL,
  `ciudad_suc` varchar(200) NOT NULL,
  `latitud_suc` double NOT NULL,
  `longitud_suc` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`id_suc`, `nombre_suc`, `telefono_suc`, `pais_suc`, `ciudad_suc`, `latitud_suc`, `longitud_suc`) VALUES
(5, 'Sucursal 1', '0980982541', 'Ecuador', 'Esmeraldas', 0.8501469705889088, -78.90314623237455),
(6, 'Sucursal 2', '0980982548', 'Ecuador', 'Zamora Chinchipe', -4.1011492917877, -78.91413256049955),
(7, 'Sucursal 3', '0980982540', 'Ecuador', 'Manabí', -1.0284287138106836, -80.68293138862455),
(8, 'Sucursal 4', '0980451236', 'Ecuador', 'Pichincha', -0.21412187135377753, -78.4609465253433),
(9, 'Sucursal 5', '0980982540', 'Colombia', 'Nariño', 1.2126364547410184, -77.27716966987455),
(10, 'Sucursal 6', '0992563125', 'Colombia', 'Cesar', 10.464964291475644, -73.24518724799955),
(11, 'Sucursal 7', '0980982540', 'Argentina', 'Río Negro', -40.843859778875476, -68.07062670112452),
(12, 'Sucursal 8', '0980451236', 'Argentina', 'Córdoba', -31.434427278274416, -64.18146654487455),
(13, 'Sucursal 9', '0992563125', 'Australia', 'Western Australia', -27.747863382740803, 114.7199006426255),
(14, 'Sucursal 10', '0980982540', 'Australia', 'Queensland', -27.47528111829563, 153.01824048637542),
(15, 'Sucursal 11', '0980982548', 'Bolivia', 'Departamento de Pando', -10.969396328515927, -66.06012865424955),
(16, 'Sucursal 12', '0444123658', 'Bolivia', 'Tarija Department', -21.438905003835593, -64.76374193549955),
(17, 'Sucursal 13', '0980982540', 'Brasil', 'Amazonas', -3.0924111394772367, -60.01764818549955),
(18, 'Sucursal 14', '0992563125', 'Brasil', 'Minas Gerais', -18.8503064283669, -41.95612474799955),
(19, 'Sucursal 15', '0980124563', 'Canadá', 'Quebec', 50.41121384758088, -73.87140795112455),
(20, 'Sucursal 16', '0992563125', 'Canadá', 'Territorio del Yucón', 64.04040586463086, -139.21808763862452),
(21, 'Sucursal 17', '0992563125', 'Colombia', 'Bolívar', 10.383926860432448, -75.44245287299955),
(22, 'Sucursal 18', '0980982540', 'Colombia', 'Amazonas', 0.05916253183086932, -71.20173021674955),
(23, 'Sucursal 19', '0980124563', 'China', 'Fujian', 26.327847796207468, 119.33137584159914),
(24, 'Sucursal 20', '0444123658', 'China', 'Xin Jiang Wei Wu Er Zi Zhi Qu', 43.932596925050035, 81.31047681450046),
(25, 'Sucursal 21', '0980982540', 'Corea del Sur', 'Jeollanam-do', 34.56434354359006, 126.60161450981292),
(26, 'Sucursal 22', '0992563125', 'Corea del Norte', 'Kangwon', 38.61588420520481, 127.98314527153167),
(27, 'Sucursal 23', '0992563125', 'Corea del Norte', 'Hwanghae del Sur', 38.02546438739006, 125.71172193168792),
(28, 'Sucursal 24', '0980451236', 'Estados Unidos', 'Washington', 47.57937982289784, -122.31012865424955),
(29, 'Sucursal 25', '0980982548', 'Corea del Norte', 'North Hamgyong', 41.272729433716805, 129.49651197075042),
(30, 'Sucursal 26', '0980124563', 'Estados Unidos', 'North Carolina', 35.20420195619401, -80.92463060737455),
(31, 'Sucursal 27', '0980124563', 'España', 'Andalucía', 37.42588593879611, -5.9649138104995725),
(32, 'Sucursal 28', '0980124563', 'España', 'Catalunya', 41.40470907663293, 2.1429963457503876),
(33, 'Sucursal 29', '0980451236', 'Francia', 'Provence-Alpes-Côte d\'Azur', 43.320263102273124, 5.3839631426254275),
(34, 'Sucursal 30', '0992563125', 'Francia', 'Île-de-France', 48.82411813715971, 2.3627229082504275),
(35, 'Sucursal 31', '0980982540', 'Japón', 'Fukuoka', 33.57238639043552, 130.43034986137542),
(36, 'Sucursal 32', '0980982540', 'Japón', 'Iwate', 39.69353623035387, 141.17497876762542),
(37, 'Sucursal 33', '0444123658', 'Estados Unidos', 'Arizona', 34.21075830473035, -112.09284349799952),
(38, 'Sucursal 34', '0980451236', 'México', 'Estado de México', 19.449863805063316, -99.03009935737455),
(39, 'Sucursal 35', '0980124563', 'Perú', 'Cuzco', -13.59916588228097, -71.95978685737455),
(40, 'Sucursal 36', '0980124563', 'Perú', 'Loreto', -3.476300323334388, -74.77228685737455),
(41, 'Sucursal 37', '0980124563', 'Rusia', 'República de Sajá', 61.982254774753855, 129.79314283012542),
(42, 'Sucursal 38', '0980982540', 'Rusia', 'Óblast de Tomsk', 59.054031902321206, 80.90398267387542),
(43, 'Sucursal 39', '0992563125', 'Ucrania', 'Volyn Oblast', 50.73217951099505, 25.335135017625426),
(44, 'Sucursal 40', '0444123658', 'Ucrania', 'Dnipropetrovs\'ka oblast', 48.40284142331082, 35.02507642387547),
(45, 'Sucursal 41', '0444123658', 'Turquía', 'Eskişehir', 39.727342617935435, 30.542654548875426),
(46, 'Sucursal 42', '0444123658', 'Turquía', 'Diyarbakır', 37.91287164575663, 40.199636970750426);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_user` int(11) NOT NULL,
  `nombre_user` varchar(150) NOT NULL,
  `apellido_user` varchar(150) NOT NULL,
  `telefono_user` varchar(10) NOT NULL,
  `email_user` varchar(150) NOT NULL,
  `password_user` varchar(150) NOT NULL,
  `perfil_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_user`, `nombre_user`, `apellido_user`, `telefono_user`, `email_user`, `password_user`, `perfil_user`) VALUES
(1, 'Jorge', 'Ante', '0980982540', 'jorge@gmail.com', '12345', 'Administrado');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cli`),
  ADD UNIQUE KEY `cedula_cli` (`cedula_cli`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id_ped`),
  ADD KEY `id_fk_cli` (`id_fk_cli`),
  ADD KEY `id_fk_suc` (`id_fk_suc`);

--
-- Indices de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD PRIMARY KEY (`id_suc`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id_ped` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  MODIFY `id_suc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`id_fk_cli`) REFERENCES `cliente` (`id_cli`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pedido_ibfk_2` FOREIGN KEY (`id_fk_suc`) REFERENCES `sucursal` (`id_suc`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
